/*global Phaser*/

// The game
var game;

// Sprites
var snow, snowballs, snowmen, eskimo, ground;

// Sounds
var walkingSound, backgroundSound;

// Some player state info
var jumpTimer = 0, spawnSnowmanTimer = 0, eskimoOnGround = false, eskimoHasBall = false, eskimoDirection = true;
var gameStateInfo, score = 0;

function main(){
    game = new Phaser.Game(800, 600, Phaser.WEBGL, 'SnowballFight', playState);
}

var playState = {

    preload: function() {
        game.load.atlasJSONHash('snowballFightSprites', 'assets/images/snowballFightSprites.png', 'assets/images/snowballFightSprites.json');
        game.load.audio('snowball', 'assets/sounds/snowball.wav');
        game.load.audio('ambientbackground', 'assets/sounds/backgroundsound.mp3');
        game.load.audio('walking', 'assets/sounds/walkinginsnow.wav');
    },
    
    create: function() {
        
        // The title of the game
        
        gameStateInfo = game.add.text(0, 0, "Hello world!", {
            font: "25px Arial", 
            fill: "white", 
            align: "center"
        });
        
        // Some setup code
        game.canvas.setAttribute('class', 'canvas');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.gravity.y = 250;
        
        // Create the ground
        ground = game.add.group();
        ground.enableBody = true;
        ground.physicsBodyType = Phaser.Physics.ARCADE;
        
        for(var i = 0; i < 25; i++){
            var groundPiece = ground.create(i * 32, 600 - 32, "snowballFightSprites");
            groundPiece.animations.add('ground', [
                5
            ], 15, false);
            groundPiece.animations.play('ground', 15, false);
            groundPiece.body.setSize(32, 32, 0, 0);
            groundPiece.body.immovable = true;
            groundPiece.body.allowGravity = false;
        }
        
        // Create the player
        eskimo = game.add.sprite(200, 200, 'snowballFightSprites');
        
        eskimo.animations.add('idle', [
            0
        ], 15, false);
        eskimo.animations.add('walk', [
            0, 1
        ], 15, false);
        eskimo.animations.add('idle_ball', [
            2
        ], 15, false);
        eskimo.animations.add('walk_ball', [
            2, 1
        ], 15, false);
        
        eskimo.animations.play('idle', 15, false);
        
        eskimo.anchor.setTo(0.5, 0.5);
        eskimo.smoothed = false;
        game.physics.arcade.enable(eskimo);
        eskimo.body.collideWorldBounds = true;
        eskimo.body.setSize(32, 64, 16, 0);
        
        // Create the snowballs group
        snowballs = game.add.group();
        snowballs.enableBody = true;
        snowballs.physicsBodyType = Phaser.Physics.ARCADE;
        
        // Create the snowmen group
        snowmen = game.add.group();
        snowmen.enableBody = true;
        snowmen.physicsBodyType = Phaser.Physics.ARCADE;
        
        // Create the background music and play it
        backgroundSound = game.add.audio('ambientbackground');
        backgroundSound.play('', 0, 1, true);
    },
    
    onEskimoGroundCollision: function(eskimo, other){
        eskimoOnGround = true;
    },
    
    onSnowballSnowmenCollision: function(ball, snowman){
        ball.kill();
        snowman.kill();
        score++;
        gameStateInfo.text = "Score: " + score;
    },
    
    onEskimoSnowmenCollision: function(){
        location.reload();
    },
    
    update: function() {
        eskimoOnGround = false;
        game.physics.arcade.collide(eskimo, ground, this.onEskimoGroundCollision, null, this);
        
        game.physics.arcade.collide(snowballs, ground);
        game.physics.arcade.collide(snowballs, snowmen, this.onSnowballSnowmenCollision, null, this);
        game.physics.arcade.collide(eskimo, snowmen, this.onEskimoSnowmenCollision, null, this);
        
        var left = game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        var right = game.input.keyboard.isDown(Phaser.Keyboard.RIGHT);
        var up = game.input.keyboard.isDown(Phaser.Keyboard.UP);
        var down = game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        var space = game.input.keyboard.justPressed(Phaser.Keyboard.SPACEBAR);
        
        eskimo.body.velocity.x = 0;
        
        if(left){
            eskimo.body.velocity.x = -100;
            eskimoDirection = false;
            eskimo.scale.x = -1;
            eskimo.animations.play('walk', 15, true);
        }
        
        if(right){
            eskimo.body.velocity.x = 100;
            eskimoDirection = true;
            eskimo.scale.x = 1;
            eskimo.animations.play('walk', 15, true);
        }
        
        if(!left && !right)
            eskimo.animations.play('idle', 15, false);
        
        if(up && eskimoOnGround && game.time.now > jumpTimer){
            eskimo.body.velocity.y = -250;
            jumpTimer = game.time.now + 750;
        }
        
        // Pickup a snowball
        if(down && eskimoOnGround && !eskimoHasBall){
            eskimoHasBall = true;
        }
        
        if(space && eskimoHasBall){
            eskimoHasBall = false;
            this.createSnowball(eskimo.x, eskimo.y, eskimoDirection);
        }
        
        if(eskimoHasBall){
            if(left || right){
                eskimo.animations.play('walk_ball', 15, true);
                console.log('walk_ball');
            }else
                eskimo.animations.play('idle_ball', 15, true);
        }
        
        
        // Spawn a snowman
        if(game.time.now > spawnSnowmanTimer){
            spawnSnowmanTimer = game.time.now + 3000;
            this.createSnowman((Math.random()<0.5));
        }
        
        
        
        //gameStateInfo.text = snowballs.countLiving() + ':' + snowmen.countLiving();
    },
    
    // 'direction' : false = left, true = right
    createSnowball: function(xPos, yPos, direction){
        var snowball = snowballs.create(xPos, yPos, "snowballFightSprites");
        snowball.animations.add('snowball', [
            6
        ], 15, false);
        snowball.animations.play('snowball', 15, false);
        snowball.smoothed = false;
        snowball.checkWorldBounds = true;
        snowball.outOfBoundsKill = true;
        snowball.body.setSize(8, 8, 0, 0);
        snowball.body.velocity.x = (direction) ? 500 : -500;
        snowball.body.velocity.y = -100;
    },
    
    createSnowman: function(direction){
        var snowman = snowmen.create((direction) ? 800 : 0, 600 - 64, 'snowballFightSprites');
        snowman.animations.add('snowball', [
            4
        ], 15, false);
        snowman.animations.play('snowball', 15, false);
        snowman.anchor.setTo(0.5, 0.5);
        snowman.smoothed = false;
        snowman.body.setSize(64, 32, 0, 0);
        snowman.body.immovable = true;
        snowman.body.allowGravity = false;
        snowman.checkWorldBounds = true;
        snowman.outOfBoundsKill = true;
        snowman.body.velocity.x = (direction) ? -100 : 100;
        snowman.scale.x = (direction) ? -1 : 1;
    },
    
    renderBody: function(b){
        game.debug.body(b);
    },
    
    render: function() {
    
        //ground.forEach(this.renderBody, this);
        //game.debug.body(eskimo);
    
    },
    
    onWalkStart: function(){
        walkingSound.play('', 0, 0.2, true);
    },
    
    onWalkEnd: function(){
        walkingSound.stop();
    },
    
    onSnowballImpact: function(){
        var snowballImpactSound = game.add.audio('snowball');
        snowballImpactSound.play('', 0, 1, false);
    }
    
}