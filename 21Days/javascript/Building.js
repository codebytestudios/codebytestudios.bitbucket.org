/*global Class game IGameObject*/

var Building = Class({implements: IGameObject}, {
    id: null,
    type: 'null',
    markedForRemoval: false,
    
    sprite: null,
    vel: {x: 0, y: 0},
    friction: {x: 0, y: 0},
    
    health: 0,
    builtPercentage: 0,
    
    __construct: function(id){
        this.id = id;
    },
    
    // Checks for collisions against all gameobjects (returns true on first hit)
    CheckCollisions: function(){
        for(var i = 0; i < game.gameobjects.length; i++){
            var gameobject = game.gameobjects[i];
            if(gameobject != this && this.sprite.rect.Intersect(gameobject.sprite.rect)) return true;
        }
        return false;
    },
    
    // Do custom game logic here
    Update: function(){
        this.sprite.Update();
    },
    
    // Do custom rendering here
    Render: function(){
        
    },
    
    // Do you custom GUI rendering here
    GUI: function(){
        
    }
});