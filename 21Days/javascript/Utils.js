function clone(obj) {
    if (null === obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

Math.clamp = function(number, min, max) {
  return Math.max(min, Math.min(number, max));
}

Math.randBool = function(){
	return ((Math.floor((Math.random() * 2) + 0) == 1) ? true : false);
}

function LoadImage(location){
	var img = new Image();
	img.src = location;
	return img;
}

function CreateMask() {
  var nMask = 0, nFlag = 0, nLen = arguments.length > 32 ? 32 : arguments.length;
  for (nFlag; nFlag < nLen; nMask |= arguments[nFlag] << nFlag++);
  return nMask;
}

function sortBySpriteYCompareFunc(a, b){
    if(a.sprite.rect.y < b.sprite.rect.y) return -1;
    else if(a.sprite.rect.y > b.sprite.rect.y) return 1;
    else return 0;
}

function RemoveDuplicates(arr){
    var good = [];
    for(var i = 0; i < arr.length; i++){
        if(good.indexOf(arr[i]) === -1) good.push(arr[i]);
    }
    return good;
}

function DrawRoundedRect(ctx, x, y, width, height, radius, fill, stroke) {
  if (typeof stroke == "undefined" ) {
    stroke = true;
  }
  if (typeof radius === "undefined") {
    radius = 5;
  }
  ctx.beginPath();
  ctx.moveTo(x + radius, y);
  ctx.lineTo(x + width - radius, y);
  ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
  ctx.lineTo(x + width, y + height - radius);
  ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
  ctx.lineTo(x + radius, y + height);
  ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
  ctx.lineTo(x, y + radius);
  ctx.quadraticCurveTo(x, y, x + radius, y);
  ctx.closePath();
  if (stroke) {
    ctx.stroke();
  }
  if (fill) {
    ctx.fill();
  }        
}

// A var storing all useful keys for easy access
var KeyCode = {
    'Backspace': 8, 'Tab': 9, 'Enter': 13, 'Shift': 16,
    'Ctrl': 17, 'Alt': 18, 'Pause': 19, 'Capslock': 20,
    'Esc': 27, 'Pageup': 33, 'Pagedown': 34, 'End': 35,
    'Home': 36, 'Leftarrow': 37, 'Uparrow': 38,
    'Rightarrow': 39, 'Downarrow': 40, 'Insert': 45, 'Delete': 46,
    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56,
    '9': 57, 'A': 65, 'B': 66, 'C': 67, 'D': 68, 'E': 69, 'F': 70, 'G': 71, 'H': 72,
    'I': 73, 'J': 74, 'K': 75, 'L': 76, 'M': 77, 'N': 78, 'O': 79, 'P': 80, 'Q': 81,
    'R': 82, 'S': 83, 'T': 84, 'U': 85, 'V': 86, 'W': 87, 'X': 88, 'Y': 89, 'Z': 90,
    'Numpad_0': 96, 'Numpad_1': 97, 'Numpad_2': 98, 'Numpad_3': 99, 'Numpad_4': 100, 
    'Numpad_5': 101, 'Numpad_6': 102, 'Numpad_7': 103, 'Numpad_8': 104, 'Numpad_9': 105,
    'Multiply': 106, 'Plus': 107, 'Minut': 109, 'Dot': 110, 'Divide': 111,
    'F1': 112, 'F2': 113, 'F3': 114, 'F4': 115, 'F5': 116, 'F6': 117, 
    'F7': 118, 'F8': 119, 'F9': 120, 'F10': 121, 'F11': 122, 'F12': 123,
    'Equal': 187, 'Coma': 188, 'Slash': 191, 'Backslash': 220
}

var MouseButton = {
    'Left' : 0,
    'Middle' : 1,
    'Right' : 2
}