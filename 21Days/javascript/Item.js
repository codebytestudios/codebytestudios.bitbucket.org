/*global Class game*/

var Item = Class({
    
    // Is this item held in an inventory?
    holder: null,
    
    equipped: false,
    
    // Sprite for displaying it in the world
    sprite: null,
    
    // position in current inventory (if held) and what part of the texture the item is
    inventoryRect: null,
    inventoryTextureRegion: null,
    
    __construct: function(inventoryRect, inventoryTextureRegion){
        this.inventoryRect = inventoryRect;
        this.inventoryTextureRegion = inventoryTextureRegion;
    },
    
    // Some items may need this ( floating particle like items for example )
    Update: function(){
        if(this.sprite) this.sprite.Update();
    },
    
    // In the game world
    Render: function(ctx){
        if(this.sprite) this.sprite.Render(ctx);
    },
    
    // Override this function in child classes, then use
    // it for things like shooting, reloading, etc.
    DoAction: function(action){
        switch(action){
            
        }
    },
    
    // In an inventory
    GUI: function(ctx){
        ctx.save();
        ctx.translate(this.inventoryRect.x * 32, this.inventoryRect.y * 32);
        ctx.drawImage(game.textures['itemsGUI'], this.inventoryTextureRegion.x, this.inventoryTextureRegion.y, this.inventoryTextureRegion.width, 
            this.inventoryTextureRegion.height, 0, 0, this.inventoryTextureRegion.width, this.inventoryTextureRegion.height);
        ctx.restore();
    }
    
});