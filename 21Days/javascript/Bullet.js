function Bullet(rect, vel){
	this.markedForRemove = false;
	this.sprite = SpriteManager.Create(rect, {x: 2, y: 2}, {x : 2, y : 2}, 0, 0, {x : true, y : true}, this.CreateAnimator("bullet"));
	this.vel = vel;
}

Bullet.prototype.CreateAnimator = function(type){
	switch(type){
		case "bullet": {
			return new Animator([
				new Animation("idle", false, 1, [new Frame(0, 105, 1, 1)]),
			]);
		} break;
	}
}

Bullet.prototype.DoPhysics = function(){
	var rect = this.sprite.rect;
	if((rect.y + rect.height) < worldRect.height - 56){
		this.vel.y += 0.0981;
	}else{
		if((rect.y + rect.height) > worldRect.height - 56) rect.y = worldRect.height - rect.height - 56;
	}
}

Bullet.prototype.Update = function(){
	this.DoPhysics();
	this.sprite.rect.x += this.vel.x;
	this.sprite.rect.y += this.vel.y;
	if(!this.sprite.rect.Intersect(worldRect)){
		this.markedForRemoval = true;
	}
}

Bullet.prototype.Render = function(){
}