/*global Class*/

var Animator = Class({
    lastTime: null,
    animations: null,
	lastAnimationId: 0,
	currentAnimationId: 0,
	currentAnimationName: "",
	lastFrame: 0,
	currentFrame: 0,
	callback: null,
	
	__construct: function(animations){
        this.lastTime = new Date().getTime();
        if(animations !== null) this.animations = animations;
	},
	
	SetAnimations: function(animations){
        this.animations = animations;
        this.currentFrame = 0;
        this.currentAnimationId = 0;
        this.currentAnimationName = "";
    },
    
    Play: function(name, callback){
        if(name != this.currentAnimationName){
            for(var i = 0; i < this.animations.length; i++){
                if(this.animations[i].name == name){
                    this.callback = (callback !== null) ? callback : null;
                    this.lastFrame = 0;
                    this.currentFrame = 0;
                    this.currentAnimationId = i;
                    this.currentAnimationName = name;
                    return;
                }
            }
            console.log("There is no animation named " + name);
        }
    },
    
    Step: function(){
        var time = new Date().getTime();
        if(time - this.lastTime >= (1000 / this.animations[this.currentAnimationId].frameRate)){
            this.lastTime = time;
            this.lastFrame = this.currentFrame;
            if(this.currentFrame < this.animations[this.currentAnimationId].frames.length - 1){
                this.currentFrame = this.currentFrame + 1;
            }else{
                this.currentFrame = 0;
                if(this.callback) this.callback();
            }
        }
    },
    
    CurrentAnimationRepeat: function(){
        return this.animations[this.currentAnimationId].repeat;
    },
    
    CurrentFramerate: function(){
        return this.animations[this.currentAnimationId].frameRate;
    },
    
    CurrentAnimationFrameCount: function(){
        return this.animations[this.currentAnimationId].frames.length;
    },
    
    LastFrameRect: function(){
        return this.animations[this.currentAnimationId].frames[this.lastFrame];
    },
    
    CurrentFrameRect: function(){
        return this.animations[this.currentAnimationId].frames[this.currentFrame];
    }
    
});

var Animation = Class({
    name: "",
    repeat: 0,
    frameRate: 0,
    frames: [],
   __construct: function(name, repeat, frameRate, frames){
        this.name = name;
        this.repeat = repeat;
        this.frameRate = frameRate;
        this.frames = frames;
    }
});

var Frame = Class({
    x: 0,
    y: 0,
    width: 0,
    height: 0,
   __construct: function(x, y, width, height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
});