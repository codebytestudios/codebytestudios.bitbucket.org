/*global Class*/

var Input = Class({
	mouse: {x: 0, y: 0},
	mouseButtons: [],
	keys: [],
	
	// These get reset every frame
	mouseButtonsPressed: [],
	mouseButtonsReleased: [],
	keysPressed: [],
	keysReleased: [],
	
	__construct: function(obj){
        if(obj.keyboard !== null) this.HookKeyboard(obj.keyboard);
        if(obj.mouse !== null) this.HookMouse(obj.mouse);
	},
	
	Update: function(){
        var i = 0;
        for(i = 0; i < this.mouseButtonsPressed.length; i++) this.mouseButtonsPressed[i] = false;
        for(i = 0; i < this.mouseButtonsReleased.length; i++) this.mouseButtonsReleased[i] = false;
        for(i = 0; i < this.keysPressed.length; i++) this.keysPressed[i] = false;
        for(i = 0; i < this.keysReleased.length; i++) this.keysReleased[i] = false;
	},
	
	HookKeyboard: function(domElement){
        var self = this;
		// Setup keyboard events
		domElement.addEventListener('keydown', function(e){
			self.keys[e.keyCode] = true;
			self.keysPressed[e.keyCode] = true;
		});
		domElement.addEventListener('keyup', function(e){
			self.keys[e.keyCode] = false;
			self.keysReleased[e.keyCode] = true;
		});
	},
	
	HookMouse: function(domElement){
        var self = this;
		// Setup mouse events
		domElement.addEventListener('mousedown', function(e){
			self.mouseButtons[e.button] = true;
			self.mouseButtonsPressed[e.button] = true;
		});
		domElement.addEventListener('mouseup', function(e){
			self.mouseButtons[e.button] = false;
			self.mouseButtonsReleased[e.button] = true;
		});
		domElement.addEventListener('mousemove', function(e){
			self.mouse.x = e.offsetX;
			self.mouse.y = e.offsetY;
		});
	},
	
	HookInput: function(domElement){
		this.HookKeyboard(domElement);
		this.HookMouse(domElement);
	},
	
	KeyPressed: function(key){
        return ((this.keysPressed[key]) ? true : false);
	},
	
	KeyReleased: function(key){
        return ((this.keysReleased[key]) ? true : false);
	},
	
	MousePressed: function(button){
        return ((this.mouseButtonsPressed[button]) ? true : false);
	},
	
	MouseReleased: function(button){
        return ((this.mouseButtonsReleased[button]) ? true : false);
	}
	
	
});