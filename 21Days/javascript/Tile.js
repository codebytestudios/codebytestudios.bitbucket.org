function Tile(rect, imgRect){
	this.rect = rect;
	this.imgRect = imgRect;
}

Tile.prototype.Render = function(){
	ctx.save();
	ctx.translate(this.rect.x, this.rect.y);
	ctx.drawImage(spriteSheet, this.imgRect.x, this.imgRect.y, this.imgRect.width, this.imgRect.height, 
		0, 0, this.rect.width, this.rect.height);
	ctx.restore();
}