/*global Class Rect game*/

var SpriteManager = {
    totalMade: 0,
    sprites: [],
    
    compareFunc: function(a, b){
        if(a.rect.y < b.rect.y) return -1;
        else if(a.rect.y > b.rect.y) return 1;
        else return 0;
    },
    
    Sort: function(){
        this.sprites.sort(this.compareFunc);
    },
    
    Update: function(){
        for(var i = 0; i < this.sprites.length; i++){
            if(this.sprites[i].layer >= 0) this.sprites[i].Update();
        }
    },
    
    // Renders all sprites
    Render: function(ctx){
        for(var i = 0; i < this.sprites.length; i++){
            if(this.sprites[i].layer >= 0) this.sprites[i].Render(ctx);
        }
    },
    
    GetIndexOfSpriteById: function(id){
        for(var i = 0; i < this.sprites.length; i++){
            if(this.sprites[i].id == id) return i;
        }
        return -1;
    },
    
    RenderObjects: function(objs, ctx){
        for(var i = 0; i < objs.length; i++){
            var index = this.GetIndexOfSpriteById(objs[i].sprite.id);
            if(index != -1){
                if(this.sprites[index].layer >= 0) this.sprites[index].Render(ctx);
            }
        }
    },
    
    Create: function(obj){
        var s = null;
        // Do we have a sprite available to use?
        for(var i = 0; i < this.sprites.length; i++){
            if(this.sprites[i].layer < 0){
                this.sprites[i]('__construct', this.sprites[i].id, obj);
                s = this.sprites[i];
                this.Sort();
                return s;
            }
        }
        // We need to create a new one
        this.totalMade++;
        this.sprites.push(new Sprite(this.totalMade, obj));
        s = this.sprites[this.sprites.length - 1];
        this.Sort();
        return s;
    },
    
    // Doesn't actually destroy it, it just causes it not to render or update
    // and makes 'adds' it to the pool for any new sprites wanted
    Destroy: function(id){
        var spliceWhat = this.GetIndexOfSpriteById(id);
        if(spliceWhat !== -1) this.sprites.splice(spliceWhat, 1);
    }
};

var Sprite = Class({
    id: null,
    rect: new Rect(0, 0, 32, 32),
    collider: { rect: new Rect(0, 0, 32, 32), offset: {x: 0, y: 0} },
    layer: 0,
    origin: {x: 16, y: 16},
    rotation: 0,
    direction: {x: false, y: false},
    //dimensions: {x: 32, y: 32},
    texture: null,
    animator: null,
    
    __construct: function(id, obj){
        this.id = id;
        if(obj.rect !== null && obj.rect.instanceOf(Rect)) this.rect = obj.rect; else this.rect = new Rect(0, 0, 32, 32);
        if(obj.collider !== null) this.collider = obj.collider; else this.collider = { rect: new Rect(0, 0, 32, 32), offset: {x: -16, y: -16} };
        //if(obj.dimensions !== null) this.dimensions = obj.dimensions; else this.dimensions = {x: 32, y: 32};
        // Default the origin to being centered on the sprite
        if(obj.origin !== null)     this.origin = obj.origin; else this.origin = {x: this.dimensions.width / 2, y: this.dimensions.height / 2};
        if(obj.rotation !== null)   this.rotation = obj.rotation; else this.rotation = 0;
        if(obj.direction !== null)  this.direction = obj.direction; else this.direction = {x: false, y: false};
        if(obj.animator !== null)   this.animator = obj.animator; else this.animator = null;
        if(obj.layer !== null)      this.layer = obj.layer; else this.layer = 0;
        if(obj.texture !== null)    this.texture = obj.texture;
        
        // We use this in ObjectTree to 
        // track where it is in the world
        obj.collider.chunks = [];
    },
    
    SetLayer: function(layer){
        this.layer = layer;
        SpriteManager.Sort();
    },
    
    GetColliderRect: function(){
        this.collider.rect.x = this.rect.x + this.collider.offset.x;
        this.collider.rect.y = this.rect.y + this.collider.offset.y;
        return this.collider.rect;
    },
    
    Update: function(){
        // Build the collision Rect
        this.GetColliderRect();
        // Step the animator
        if(this.animator !== null) this.animator.Step();
    },
    
    Render: function(ctx){
        // Render the current frame
        if(this.animator !== null && this.texture !== null){
            var frame = this.animator.CurrentFrameRect();
            
            ctx.save();
            ctx.translate(this.rect.x + this.origin.x, this.rect.y + this.origin.y);
            ctx.rotate(this.rotation * (Math.PI/180.0));
            ctx.scale((this.direction.x === true) ? -1 : 1, (this.direction.y === true) ? -1 : 1);
            ctx.drawImage(this.texture, frame.x, frame.y, frame.width, frame.height, 
                -this.origin.x, -this.origin.y, this.rect.width, this.rect.height);
            ctx.restore();
        }
    }
    
});