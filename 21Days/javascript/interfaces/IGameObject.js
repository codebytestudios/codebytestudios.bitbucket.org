/*global Interface*/

// Anything that wants to update and 
// render must extend this interface
var IGameObject = Interface({
    id: 'string',
    type: 'string',
	markedForRemoval: 'boolean',
	Update: 'function',
	Render: 'function'
});