/*global Class CreateMask*/

/*

    16 bit format: rrr bbb ggg aaa oooo
    
    3 bits per color channel = 12 bits
    4 extra bits we can use  = 4 bits
    
    This allows for 4096 different colors (I think)

    Since colors are usually 8 bits (256 possible values)
    we'll have to use (256 * ( 0 / 7 )) to bring the 3 bit
    color value in range (8 possible values)

*/


var LightingSystem = Class({
    width: 0,
    height: 0,
    data: null,
    reader: null,
    
    canvas: null,
    ctx: null,
    imgData: null,
    
    __construct: function(width, height, ctx){
        this.width = width;
        this.height = height;
        this.data = new ArrayBuffer(width * height);
        this.reader = new Uint16Array(this.data);
        
        // We render the final lighting to this 
        // (then render this canvas to the main one)
        this.canvas = document.createElement("canvas");
        this.canvas.width = width;
        this.canvas.height = height;
        this.ctx = this.canvas.getContext('2d');
        this.imgData = ctx.createImageData(this.width, this.height);
        
        // Give each part a default value of 0000000000000000 
        for(var i = 0; i < this.data.byteLength; i++){
            this.reader[i] = 0x00;
        }
    },
    
    // x: 0 - width, y: 0 - height, which: 0 - 16, val: true or false
    SetBit: function(x, y, which, val){
        var mask = 1 << which;
        if(val === 1) this.reader[y * this.width + x] |= mask;
        else if(val === 0) this.reader[y * this.width + x] &= ~mask;
    },
    
    // Same as above
    GetBit: function(x, y, which){
        var mask = 1 << which;
        if((this.reader[y * this.width + x] & mask) !== 0){
            return 1;
        }else{
            return 0;
        }
    },
    
    // x: 0 - width, y: 0 - height, color: [r: 0 - 7, g: 0 - 7, b: 0 - 7, a: 0 - 7]
    SetColor: function(x, y, color){
        for(var i = 0; i < 4; i++){
            for(var ii = 0; ii < 3; ii++){
                this.SetBit(x, y, (i * 3 + ii), ((color[i] & (1 << ii)) !== 0) ? 1 : 0);
            }
        }
    },
    
    // This will be called to render the lighting to the screen
    Render: function(ctx, canvas){
        var r, g, b, a;
        for(var y = 0; y < this.height; y++){
            for(var x = 0; x < this.width; x++){
                var chunk = this.reader[y * this.width + x];
                    r = CreateMask(this.GetBit(x, y, 0), this.GetBit(x, y, 1), this.GetBit(x, y, 2));
                    g = CreateMask(this.GetBit(x, y, 3), this.GetBit(x, y, 4), this.GetBit(x, y, 5));
                    b = CreateMask(this.GetBit(x, y, 6), this.GetBit(x, y, 7), this.GetBit(x, y, 8));
                    a = CreateMask(this.GetBit(x, y, 9), this.GetBit(x, y, 10), this.GetBit(x, y, 11));
                this.imgData.data[(y * this.width + x) + 0] = 255;
                this.imgData.data[(y * this.width + x) + 1] = 0;
                this.imgData.data[(y * this.width + x) + 2] = 0;
                this.imgData.data[(y * this.width + x) + 3] = 0;
            }
        }
        //this.ctx.putImageData(this.imgData, 0, 0);
        //ctx.drawImage(this.canvas, 0, 384);
    }
    
    
});