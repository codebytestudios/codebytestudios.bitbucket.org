/*global Class Rect debug sortBySpriteYCompareFunc*/

var ObjectTree = Class({
    width: 0,
    height: 0,
    objects: [],
    map: [],
    bounds: null,
    
    __construct: function(width, height, chunksize){
        this.width = width;
        this.height = height;
        this.chunksize = chunksize;
        this.bounds = new Rect(0, 0, (this.width * this.chunksize) - 1, (this.height * this.chunksize) - 1);
        for(var i = 0; i < (this.width * this.height); i++) this.map[i] = [];
    },
    
    GetChunkPos: function(pos){
        return {x: Math.floor(pos.x / this.chunksize), y: Math.floor(pos.y / this.chunksize)};
    },
    
    // returns an array of objects in that chunk
    GetObjectsInChunk: function(pos){
        // Make sure its in the bounds of the map
        if(pos.x >= 0 && pos.x < this.width && pos.y >= 0 && pos.y < this.height){
            return this.map[pos.y * this.width + pos.x];
        }else{
            if(debug == 2) console.log("[ERROR] ObjectTree.GetObjectsInChunk: " + pos.x + "," + pos.y + " out of bounds!");
            return [];
        }
    },
    
    SortChunk: function(pos){
        this.map[pos.y * this.width + pos.x].sort(sortBySpriteYCompareFunc);
    },
    
    AddObject: function(obj){
        if(this.objects.indexOf(obj) == -1){
            this.objects.push(obj);
            this.UpdateObject(obj);
        }else{
            if(debug == 2) console.log('[ERROR] ObjectTree.AddObject: This object has already been added!');
        }
    },
    
    // updates the objects
    UpdateObject: function(obj){
        if(this.objects.indexOf(obj) > -1){
            // remove the object from the chunks they were last in
            var len = obj.sprite.collider.chunks.length;
            var objPrevChunks = obj.sprite.collider.chunks;
            for(var i = 0; i < len; i++){
                var arr = this.map[objPrevChunks[i].y * this.width + objPrevChunks[i].x];
                var index = arr.indexOf(obj);
                arr.splice(index, 1);
            }
            
            var chunks = this.BakeObject(obj.sprite.GetColliderRect());
            
            if(chunks !== null){
                for(var ii = 0; ii < chunks.length; ii++){
                    this.map[chunks[ii].y * this.width + chunks[ii].x].push(obj);
                }
                obj.sprite.collider.chunks = chunks;
            }else{
                if(debug == 2) console.log("[ERROR] ObjectTree.UpdateObject: object out of map bounds");
            }
        }else{
            if(debug == 2) console.log("[ERROR] ObjectTree.UpdateObject: object hasnt been added yet!");
        }
        
    },
    
    // takes each corner of an rect and checks what chunk its in
    // if the obj has not already been added to that chunk, add it. typically
    // an object can only be a max of 4 chunks (since it only has 4 corners)
    BakeObject: function(objRect){
        var chunksToAddTo = [];
        
        // Get all the points that need to be checked (each corner)
        var points = objRect.GetCorners();
        
        // Now check each of those
        for(var i = 0; i < 4; i++){
            // Make sure its in bounds
            if(points[i].x >= 0 && points[i].x < (this.width * this.chunksize) && 
                points[i].y >= 0 && points[i].y < (this.height * this.chunksize)){
                    
                // Get the chunk coords we should add it to
                var pX = Math.floor(points[i].x / this.chunksize);
                var pY = Math.floor(points[i].y / this.chunksize);
                var addit = true;
                
                // Make sure we havent already added it to that chunk
                for(var ii = 0; ii < chunksToAddTo.length; ii++){
                    // Its already been added, set addit to false and break
                    if(chunksToAddTo[ii].x == pX && chunksToAddTo[ii].y == pY){
                        addit = false; break;
                    }
                }
                
                // Only add it to chunksToAddTo if addit is true
                if(addit === true){
                    chunksToAddTo.push(
                        {x: Math.floor(points[i].x / this.chunksize), y: Math.floor(points[i].y / this.chunksize)}
                    );
                }
                
            }else{
                // We got a bad point! Return immediately
                return null;
            }
        }
        
        
        return chunksToAddTo;
    },
    
    RebuildMap: function(){
        
    },
    
    DebugDrawMap: function(ctx){
        ctx.save();
        
        ctx.lineWidth = "1";
        ctx.strokeStyle = "black";
        ctx.font = "20px Georgia";
        ctx.fillStyle = "black";
        
        for(var x = 0; x < this.width + 1; x++){
            ctx.beginPath();
            ctx.moveTo(x * this.chunksize, 0);
            ctx.lineTo(x * this.chunksize, this.height * this.chunksize);
            ctx.stroke();
        }
        
        for(var y = 0; y < this.height + 1; y++){
            ctx.beginPath();
            ctx.moveTo(0, y * this.chunksize);
            ctx.lineTo(this.width * this.chunksize, y * this.chunksize);
            ctx.stroke();
        }
        
        for(var yy = 0; yy < this.height; yy++){
            for(var xx = 0; xx < this.width; xx++){
                ctx.fillText(this.map[yy * this.width + xx].length, (xx * this.chunksize) + (this.chunksize / 2), (yy * this.chunksize) + (this.chunksize / 2));
            }
        }
        
        ctx.restore();
    }
    
});