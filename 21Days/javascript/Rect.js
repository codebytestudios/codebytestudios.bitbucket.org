/*global Class*/

var Rect = Class({
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    
    __construct: function(x, y, width, height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    },
    
    CheckPoint: function(other){
        if ((other.x > this.x && other.x < (this.x + this.width)) &&
        (other.y > this.y && other.y < (this.y + this.height)))
            return true;
            
        return false;
    },
    
    Intersect: function(other){
        if (this.x < (other.x + other.width) && (this.x + this.width) > other.x &&
        this.y < (other.y + other.height) && (this.y + this.height) > other.y)
            return true;
            
        return false;
    },
    
    ForceInside: function(other){
        if(this.x < other.x) this.x = other.x;
        if(this.x + this.width > other.x + other.width) this.x = other.x + other.width - this.width;
        if(this.y < other.y) this.y = other.y;
        if(this.y + this.height > other.y + other.height) this.y = other.y + other.height - this.height;
    },
    
    ForceOutside: function(other){
        if(this.Intersect(other)){
            if(this.x < other.x) this.x = other.x - this.width;
            if(this.x > other.x) this.x = other.x + other.width;
            if(this.y < other.y) this.y = other.y - this.height;
            if(this.y > other.y) this.y = other.y + other.height;
        }
    },
    
    GetCorners: function(){
        return [
            {x: this.x, y: this.y},                             // topLeft
            {x: this.x + this.width, y: this.y},                // topRight
            {x: this.x, y: this.y + this.height},               // bottomLeft
            {x: this.x + this.width, y: this.y + this.height}   // bottomRight
        ]
    }
    
});