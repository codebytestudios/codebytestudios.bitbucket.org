/*global Class Rect MouseButton debug*/

// An Inventory can range anywhere from the players backpack to a cars storage
var Inventory = Class({
    screenRect: null, // We check if the mouse is in this when dragging an item
    width: 0,
    height: 0,
    items: [],
    map: [],
   
    __construct: function(screen_x, screen_y, width, height){
        this.screenRect = new Rect(screen_x, screen_y, width * 32, height * 32);
        
        this.width = width;
        this.height = height;
        for(var i = 0; i < (this.width * this.height); i++) this.map[i] = false;
    },
   
    // Trys to set the item at x,y
    AddItem: function(item){
        if(item.inventoryRect.instanceOf(Rect)){
            if(this.SpaceFree(item.inventoryRect)){
                item.holder = this;
                this.items.push(item);
                this.BuildMap();
                return true;
            }else{
                console.log("Item won't fit!");
                return false;
            }
        }else{
            console.log("Could not add item!");
            return false;
        }
    },
    
    RemoveItem: function(item){
        var index = this.items.indexOf(item);
        
        if(index != -1){
            item.holder = null;
            this.items.splice(index, 1);
            this.BuildMap();
            return true;
        }else{
            console.log("Item doesn't exist in this inventory!");
            return false;
        }
    },
    
    SpaceFree: function(rect){
        // make sure its a valid rect
        if(rect.x < 0 || rect.y < 0 || rect.width <= 0 || rect.height <= 0) return false;
        
        // the rect can also not be outside the map
        if(rect.x + rect.width > this.width || rect.y + rect.height > this.height) return false;
        
        // Is that spot available in the map?
        for(var y = rect.y; y < rect.height + rect.y; y++){
            for(var x = rect.x; x < rect.width + rect.x; x++){
                if(this.map[y * this.width + x] === true) return false;
            }
        }
        return true;
    },
    
    // Returns a rect with the free space if it can find one
    // returns null if it can't
    FindFreeSpace: function(width, height){
        var tempRect = new Rect(0, 0, width, height);
        for(var y = 0; y < this.height; y++){
            for(var x = 0; x < this.width; x++){
                if(this.map[y * this.width + x] === false){
                    tempRect.x = x;
                    tempRect.y = y;
                    if(this.SpaceFree(tempRect)){
                        return tempRect;
                    }
                }
            }
        }
        
        // Return a bad rect
        tempRect.x = -1;
        tempRect.x = -1;
        tempRect.width = 0;
        tempRect.height = 0;
        
        return tempRect;
    },
    
    PosInItem: function(pos, itemRect){
        for(var y = itemRect.y; y < itemRect.y + itemRect.height; y++){
            for(var x = itemRect.x; x < itemRect.x + itemRect.width; x++){
                if(x == pos.x && y == pos.y) return {x: itemRect.x - x, y: itemRect.y - y};
            }
        }
        return null;
    },
    
    ItemAt: function(pos){
        if(pos.x >= 0 && pos.x < this.width && pos.y >= 0 && pos.y < this.height){
            for(var i = 0; i < this.items.length; i++){
                var relPos = this.PosInItem(pos, this.items[i].inventoryRect);
                if(relPos) return {item: this.items[i], relPos: relPos};
            }
            return null;
        }
        return null;
    },
    
    // Builds a table of where an item can and cant be placed
    // 1's are taken, 0's are free space, heres an example:
    // [1, 1, 0, 0]
    // [1, 1, 0, 0]
    // [0, 0, 0, 0]
    // [0, 0, 0, 1]
    BuildMap: function(){
        var map = [];
        for(var i = 0; i < (this.width * this.height); i++) map[i] = false;
        
        // For each item
        for(var ii = 0; ii < this.items.length; ii++){
            var item = this.items[ii];
            
            // Add these values to the map
            for(var y = item.inventoryRect.y; y < item.inventoryRect.height + item.inventoryRect.y; y++){
                for(var x = item.inventoryRect.x; x < item.inventoryRect.width + item.inventoryRect.x; x++){
                    map[y * this.width + x] = true;
                }
            }
        }
        
        // Debug draw it to the console
        if(debug == 2){
            var s = "";
            for(var yy = 0; yy < this.height; yy++){
                for(var xx = 0; xx < this.width; xx++){
                    s += (((map[yy * this.width + xx]) ? '1' : '0') + ' ');
                }
                s += '\n';
            }
            console.log(s);
        }
        
        this.map = map;
    },
    
    // Modifie's container by putting an item and offset in it
    // if we grabbed an item, then returns it back for further use
    Update: function(input, container){
        var relPos = {x: input.mouse.x - this.screenRect.x, y: input.mouse.y - this.screenRect.y};
        var slotPos = {x: Math.floor(relPos.x / 32), y: Math.floor(relPos.y / 32)};
        
        if(this.screenRect.CheckPoint(input.mouse)){
            if(input.MousePressed(MouseButton.Left)){
                container = this.ItemAt(slotPos);
            }
            
            if(input.MouseReleased(MouseButton.Left)){
                if(container){
                    if(container.item.holder == this){
                        this.RemoveItem(container.item);
                    }else{
                        container.item.holder.RemoveItem(container.item);
                    }
                    container.item.inventoryRect.x = slotPos.x + container.relPos.x;
                    container.item.inventoryRect.y = slotPos.y + container.relPos.y;
                    this.AddItem(container.item);
                    container = null;
                }
            }
            
        }
        
        return container;
    },
    
    Render: function(ctx){
        for(var i = 0; i < this.items.length; i++){
            this.items[i].Render(ctx);
        }
    },
    
    GUI: function(ctx){
        
        ctx.save();
        
        ctx.translate(this.screenRect.x, this.screenRect.y);
        
        ctx.lineWidth = "1";
        ctx.strokeStyle = "black";
        
        ctx.beginPath();
        ctx.rect(0.5, 0.5, this.width * 32, this.height * 32);
        ctx.stroke();
        
        ctx.beginPath();
        
        for(var x = 0; x < this.width; x++){
            ctx.moveTo(x * 32 + 0.5, 0.5);
            ctx.lineTo(x * 32 + 0.5, this.height * 32 + 0.5);
        }
        
        for(var y = 0; y < this.height; y++){
            ctx.moveTo(0.5, y * 32 + 0.5);
            ctx.lineTo(this.width * 32 + 0.5, y * 32 + 0.5);
        }
        
        ctx.stroke();
        
        /*
        ctx.fillStyle = "red";
        for(var yy = 0; yy < this.height; yy++){
            for(var xx = 0; xx < this.width; xx++){
                if(this.map[yy * this.width + xx] === true){
                    ctx.fillRect(xx * 32, yy * 32, 32, 32);
                }
            }
        }
        */
        
        for(var i = 0; i < this.items.length; i++){
            this.items[i].GUI(ctx);
        }
        
        ctx.restore();
        
        
    }
});