/*global Class LoadImage IGameObject SpriteManager KeyCode Player Fireplace Zombie ObjectTree RemoveDuplicates Stats Item Rect Input Pistol sortBySpriteYCompareFunc LightingSystem*/

// debug: 0 = none, 1 = normal, 2 = strict
var game, debug = 0;

var Game = Class({
	canvas: null,
	ctx: null,
	gameobjects: [],
	input: null,
	textures: {},
	camera: {x : -1, y : -1},
	
	world: null,
	lightingSystem: null,
	player: null,
	
	// This stuff is used for delta time
	deltaTime: 0,
	lastTime: 0,
	
	stats: null,

	__construct: function(){
		this.canvas = document.getElementById("game");
		this.ctx = this.canvas.getContext('2d');
		this.ctx.imageSmoothingEnabled = false;
	
        // A stat counter for FPS
        this.stats = new Stats();
        this.stats.setMode(0); // 0: fps, 1: ms
        
        // Align top-left
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.left = '0px';
        this.stats.domElement.style.top = '0px';
        
        document.body.appendChild( this.stats.domElement );
	
        this.world = new ObjectTree(32, 32, 512);
        this.lightingSystem = new LightingSystem(768, 768, this.ctx);
        
        for(var y = 0; y < this.lightingSystem.height; y++){
           for(var x = 0; x < this.lightingSystem.width; x++){
                this.lightingSystem.SetColor(x, y, [0, 0, 0, 0]);
            } 
        }
        
        //this.lightingSystem.SetBit(0, 0, 0, 1);
        //this.lightingSystem.SetBit(0, 0, 1, 1);
        //this.lightingSystem.SetBit(0, 0, 2, 1);
        
        //this.lightingSystem.Render(this.ctx, this.canvas);
	
		this.input = new Input({
			keyboard: window,
			mouse: this.canvas
		});
		
		this.textures['characters'] = LoadImage("assets/textures/characters.png");
		this.textures['buildings'] = LoadImage("assets/textures/buildings.png");
		this.textures['map'] = LoadImage("assets/textures/map.png");
		this.textures['backpack'] = LoadImage("assets/textures/backpack.png");
		this.textures['itemsGUI'] = LoadImage("assets/textures/itemsGUI.png");
		this.textures['itemsWorld'] = LoadImage("assets/textures/itemsWorld.png");
	},
	
	AddGameObject: function(obj){
		// Make sure it implements IGameObject
		if(obj.instanceOf(IGameObject)){
            this.gameobjects.push(obj);
            this.world.AddObject(obj);
            return obj;
		}else console.log("Not GameObject!");
	},
	
	GetGameObjectById: function(id){
        for(var i = 0; i < this.gameobjects.length; i++) {
            if(this.gameobjects[i].id == id) return this.gameobjects[i];
        }
        return null;
	},
	
	Start: function(){
        this.player = this.AddGameObject(new Player(800, 800));
        
        this.player.inventory.AddItem(
            new Pistol(new Rect(1, 0, 1, 2))
        );
        
        this.player.inventory.AddItem(
            new Pistol(new Rect(0, 0, 1, 2))
        );
        
        /*
        this.player.inventory.AddItem(
            new Item(
                new Rect(0, 2, 1, 2), 
                new Rect(0, 0, 32, 64)
            )
        );
        
        this.player.inventory.AddItem(
            new Item(
                new Rect(1, 0, 1, 4), 
                new Rect(32, 0, 32, 128)
            )
        );
        */
        
        //this.AddGameObject(new Zombie('zombie_0', 400, 400));
        var len = 5;
        for(var y = 0; y < len; y++){
            for(var x = 0; x < len; x++){
                this.AddGameObject(new Zombie('zombie_' + (y * len + x), (64 * x) + 16, (64 * y) + 16));
            }
        }
        
        this.AddGameObject(new Fireplace('fireplace_0', 264, 264));
        this.Loop();
	},
	
	Loop: function(){
        this.stats.begin();

		window.requestAnimationFrame(this.Loop.bind(this));
		var rect = this.player.sprite.GetColliderRect();
		var pos = this.world.GetChunkPos({x: rect.x, y: rect.y});
		var objs = [];
		
		for(var y = -1; y < 2; y++){
            for(var x = -1; x < 2; x++){
                objs = objs.concat(this.world.GetObjectsInChunk({x: pos.x + x, y: pos.y + y}, false));
                //objs = temp;
            }
		}
		objs = RemoveDuplicates(objs);
		objs.sort(sortBySpriteYCompareFunc);
		
		this.Update(objs);
		this.Render(objs);
		
		this.stats.end();
	},
	
	RemoveObjects: function(){
        for(var i = 0; i < this.gameobjects.length; i++) {
			// Remove it if its marked for removal
            if(this.gameobjects[i].markedForRemoval === true) { 
                SpriteManager.Destroy(this.gameobjects[i].sprite.id); 
                this.gameobjects.splice(i, 1);
            }
		}
	},
	
	Update: function(objs){
        
        var currentTime = new Date().getTime();
        this.deltaTime = currentTime - this.lastTime;
        this.lastTime = currentTime;
        
		SpriteManager.Update();
	
		// Update everything
		for(var i = 0; i < objs.length; i++) {
			// Update it
			objs[i].Update();
		}
		
		// Removes all objects marked for removal
		this.RemoveObjects();
		
		//this.camera = {x: Math.round(-this.player.sprite.rect.x + 384 - 32), y: Math.round(-this.player.sprite.rect.y + 384 - 32)};
		
		// With walking space (before camera movement)
		//this.camera.x += Math.floor(((-this.player.sprite.rect.x + 384 - 32) - this.camera.x) / 40.0);
		//this.camera.y += Math.floor(((-this.player.sprite.rect.y + 384 - 32) - this.camera.y) / 40.0);
		
		// Without walking space (before camera movement)
		this.camera.x += ((-this.player.sprite.rect.x + 384 - 32) - this.camera.x) / 40.0;
		this.camera.y += ((-this.player.sprite.rect.y + 384 - 32) - this.camera.y) / 40.0;
		
		//this.camera.x = Math.round(this.camera.x);
		//this.camera.y = Math.round(this.camera.y);
		
		//SpriteManager.Sort();
		this.input.Update();
	},
	
	Render: function(objs){
        this.ctx.save();

		//this.ctx.fillStyle = "white";
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		
		this.ctx.translate(this.camera.x, this.camera.y);
		
		for(var y = 0; y < 20; y++){
            for(var x = 0; x < 20; x++){
                // We have to render the textures one pixel longer to avoid tearing between textures
                this.ctx.drawImage(this.textures['map'], 0, 0, 64, 64, x * 128, y * 128, 129, 129);
            }
		}
		
		SpriteManager.RenderObjects(objs, this.ctx);
		
		for(var i = 0; i < objs.length; i++)
			objs[i].Render(this.ctx);
			
		if(debug){
            this.ctx.lineWidth = "1";
            this.ctx.strokeStyle = "black";
            for(var ii = 0; ii < this.gameobjects.length; ii++){
                this.ctx.save();
                
                var rect = this.gameobjects[ii].sprite.GetColliderRect();
                
                this.ctx.beginPath();
                this.ctx.rect(rect.x, rect.y, rect.width, rect.height);
                this.ctx.stroke();
                
                this.ctx.restore();
            }
            
            this.world.DebugDrawMap(this.ctx);
		}
		
		this.ctx.restore();
		
		//this.lightingSystem.Render(this.ctx, this.canvas);
		
		for(var iii = 0; iii < this.gameobjects.length; iii++)
			this.gameobjects[iii].GUI();
	}
	
});

// Main code
function Main(){
	game = new Game();
	game.Start();
}