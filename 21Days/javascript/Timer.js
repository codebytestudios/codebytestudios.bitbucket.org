/*global Class*/

var Timer = Class({
	lastTime: null,
	callback: null,
	interval: null,
	active: null,
	obj: null,
	
	__construct: function(callback, interval, active, obj){
        this.lastTime = new Date().getTime();
        this.callback = callback;
        this.interval = interval;
        this.active = active;
        this.obj = obj;
	},
	
    Tick: function(){
        if(this.active){
            var time = new Date().getTime();
            if(time - this.lastTime >= this.interval){
                this.callback(this.obj, this);
                this.lastTime = time;
            }
        }
    },
    
    SetActive: function(active){
        if(active != this.active){
            this.lastTime = new Date().getTime();
            this.active = active;
        }
    },
    
    Reset: function(){
        this.lastTime = new Date().getTime();
    }
    
});