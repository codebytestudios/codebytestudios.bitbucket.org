/*global Class Item Rect SpriteManager Animator Animation Frame game*/

var Pistol = Class({extends: Item}, {
    
    __construct: function(inventoryRect){
        this.sprite = SpriteManager.Create({
            rect: new Rect(0, 0, 32, 32),
            collider: {rect: new Rect(0, 0, 32, 32), offset: {x: 0, y: 0}},
            origin: {x: 16, y: 32},
            layer: 300,
            rotation: 0,
            direction: {x: false, y: false},
            texture: game.textures['itemsWorld'],
            animator: new Animator([
				new Animation("idle", false, 10, [ new Frame(0, 0, 32, 32)])
			])
        });
        this.sprite.animator.Play('idle');
        
        this['super']('__construct', inventoryRect, new Rect(0, 0, 32, 64));
    },
    
    Update: function(){
        this['super']('Update');
    },
    
    Render: function(ctx){
        this['super']('Render', ctx);
    }
    
});