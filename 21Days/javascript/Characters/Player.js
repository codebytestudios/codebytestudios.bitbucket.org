/*global Class Character Sprite Animator Animation Frame SpriteManager Rect game KeyCode Inventory MouseButton*/

var Player = Class({extends: Character}, {
    inventory: null,
    inventoryOpen: false,
    
    handInventory: null,
    itemContainer: null,
    
    moving: false,
    
    __construct: function(x, y){
        var motionFrames = [
            new Frame(0, 0, 32, 32),
            new Frame(96, 0, 32, 32),
            new Frame(128, 0, 32, 32),
            new Frame(96, 0, 32, 32),
            new Frame(0, 0, 32, 32),
            new Frame(64, 0, 32, 32),
            new Frame(32, 0, 32, 32),
            new Frame(64, 0, 32, 32)
        ]
        
        this.sprite = SpriteManager.Create({
            rect: new Rect(x, y, 64, 64),
            collider: {rect: new Rect(0, 0, 32, 32), offset: {x: 16, y: 32}},
            //dimensions: {width: 64, height: 64}, 
            origin: {x : 32, y : 32}, 
            layer: 200,
            rotation: 0,
            direction: {x: false, y: false},
            texture: game.textures['characters'],
            animator: new Animator([
				new Animation("idle", false, 1, [new Frame(0, 0, 32, 32)]),
				new Animation("walk", false, 10, motionFrames),
				new Animation("run", false, 15, motionFrames),
				new Animation("backpack", false, 15, [new Frame(192, 0, 32, 32)])
			])
        });
        this.sprite.animator.Play('idle');
        this.speed = 0.1;
        this.health = 100.0;
        this.stamina = 100.0;
        
        this.inventory = new Inventory(384 - ((32 * 6) / 2), 384 - ((32 * 8) / 2), 6, 8);
        this.handInventory = new Inventory(384 - ((32 * 6) / 2) + 224, 384 - ((32 * 8) / 2), 1, 4);
        
        this['super']('__construct', 'Player');
    },
    
    Update: function(){
        this.moving = false;
        var running = false;
        
        if(!this.inventoryOpen){
            
            var speed = {x: 0, y: 0};
        
            // Simple WASD keyboard movement
            if(game.input.keys[KeyCode.W]){
                speed.y -= 1;
                this.moving = true;
            }
            if(game.input.keys[KeyCode.S]){
                speed.y += 1;
                this.moving = true;
            }
            if(game.input.keys[KeyCode.A]){
                speed.x -= 1;
                this.sprite.direction.x = true;
                this.moving = true;
            }
            if(game.input.keys[KeyCode.D]){
                speed.x += 1;
                this.sprite.direction.x = false;
                this.moving = true;
            }
            
            // Sprint if shift is pressed and you are not out of stamina
            if(this.moving && game.input.keys[KeyCode.Shift] && this.stamina > 0){
                if(this.speed < 0.2) this.speed += 0.0002 * game.deltaTime;
                this.stamina -= 0.025 * game.deltaTime;
                running = true;
            }else if(this.stamina < 100){
                this.speed = 0.1;
            }
            
            // normalize the speed
            var length = Math.sqrt(speed.x * speed.x + speed.y * speed.y);
            speed.x /= length;
            speed.y /= length;
            speed.x *= this.speed * game.deltaTime;
            speed.y *= this.speed * game.deltaTime;
            
            // Now apply it
            if(!isNaN(speed.x)) this.sprite.rect.x += speed.x;
            if(!isNaN(speed.y)) this.sprite.rect.y += speed.y;
            
            this.sprite.rect.ForceInside(game.world.bounds);
            
            if(this.moving){
                if(running) this.sprite.animator.Play('run');
                else this.sprite.animator.Play('walk');
                game.world.UpdateObject(this);
            }else{
                this.sprite.animator.Play('idle');
            }
        
        }else{
            this.sprite.animator.Play('backpack');
        }
        
        // Only regain stamina if where not pressing shift
        if(!game.input.keys[KeyCode.Shift] && this.stamina < 100){
            if(this.moving){
                this.stamina += 0.01 * game.deltaTime;
            }
            // We gain stamina back faster if where not moving
            else{
                this.stamina += 0.025 * game.deltaTime;
            }
        }
        
        // Press the keyboard key 'Q' to open the inventory
        if(game.input.KeyReleased(KeyCode.Q))
            this.inventoryOpen = !this.inventoryOpen;
            
        if(this.inventoryOpen){
            this.itemContainer = this.handInventory.Update(game.input, this.itemContainer);
            this.itemContainer = this.inventory.Update(game.input, this.itemContainer);
        }
            
        // Round stamina down or up to always equal 0 or 100
        if(this.stamina < 0) this.stamina = 0;
        if(this.stamina > 100) this.stamina = 100;
        
        for(var i = 0; i < this.handInventory.items.length; i++){
            var s = this.handInventory.items[i].sprite;
            s.rect.x = this.sprite.rect.x + s.origin.x + ((this.sprite.direction.x) ? -16 * ((i === 0) ? 1 : 0) : 16 * ((i === 0) ? 1 : 0));
            s.rect.y = this.sprite.rect.y + s.origin.y - 4;
            s.direction = this.sprite.direction;
            s.layer = this.sprite.layer;
        }
        
        this['super']('Update');
    },
    
    Render: function(ctx){
        
        if(!this.moving && !this.inventoryOpen){
            this.handInventory.Render(ctx);
            //this.inventory.Render(ctx);
        }
        
        this['super']('Render');
    },
    
    // Do you custom GUI rendering here
    GUI: function(){
        
        // Render the health and stamina
        game.ctx.beginPath();
        game.ctx.lineWidth = "1";
        game.ctx.strokeStyle = "black";
        game.ctx.rect(8.5, 8.5, 128, 16); 
        game.ctx.stroke();
        
        game.ctx.beginPath();
        game.ctx.lineWidth = "1";
        game.ctx.strokeStyle = "black";
        game.ctx.rect(8.5, 33.5, 128, 16); 
        game.ctx.stroke();
        
        game.ctx.fillStyle = "rgba(255, 0, 0, 0.5)";
        game.ctx.fillRect(8.5, 8.5, 128 / (100 / this.health), 16);
        
        game.ctx.fillStyle = "rgba(0, 0, 255, 0.5)";
        game.ctx.fillRect(8.5, 33.5, 128 / (100 / this.stamina), 16);
        
        //game.ctx.lineWidth = "4";
        //DrawRoundedRect(game.ctx, 100.5, 100.5, 100, 100, 15, true, true);
        
        if(this.inventoryOpen) {
            game.ctx.setTransform(1, 0, 0, 1, 0, 0);
            game.ctx.globalAlpha = 0.5;
            game.ctx.drawImage(game.textures['backpack'], 384 - 256, 384 - 256, 512, 512);
            game.ctx.globalAlpha = 1;
            this.handInventory.GUI(game.ctx);
            this.inventory.GUI(game.ctx);
        }
        
    }
    
});









