/*global Class Character Sprite Animator Animation Frame SpriteManager Rect game KeyCode Timer*/

var Zombie = Class({extends: Character}, {
    canAttack: true,
    hitTimer: null,
    
    __construct: function(id, x, y){
        this.sprite = SpriteManager.Create({
            rect: new Rect(x, y, 64, 64), 
            collider: {rect: new Rect(0, 0, 32, 32), offset: {x: 16, y: 32}},
            //dimensions: {width: 64, height: 64}, 
            origin: {x : 32, y : 32},
            layer: 60, 
            rotation: 0, 
            direction: {x: false, y: false},
            texture: game.textures['characters'],
            animator: new Animator([
				new Animation("idle", false, 1, [ new Frame(0, 32, 32, 32), new Frame(160, 32, 32, 32)]),
				new Animation("walk", false, 10, [
                    new Frame(0, 32, 32, 32),
                    new Frame(96, 32, 32, 32),
                    new Frame(128, 32, 32, 32),
                    new Frame(96, 32, 32, 32),
                    new Frame(0, 32, 32, 32),
                    new Frame(64, 32, 32, 32),
                    new Frame(32, 32, 32, 32),
                    new Frame(64, 32, 32, 32)
				])
			])
        });
        this.sprite.animator.Play('idle');
        this.speed = 0.06;
        this.health = 100.00;
        
        this.hitTimer = new Timer(function(obj, timer){
            // This function gets called back every second
            obj.canAttack = true;
        }, 1000, true, this);
        
        this['super']('__construct', id);
    },
    
    Update: function(){
        // Declare and set variables
        var mouse = game.input.mouse;
        var playerPos = {x: game.player.sprite.rect.x, y: game.player.sprite.rect.y};
        var zombiePos = {x: this.sprite.rect.x, y: this.sprite.rect.y};
        
        // Calculate the diffence between player_x/y and zombies_x/y
        var diff = {x: playerPos.x - zombiePos.x, y: playerPos.y - zombiePos.y};
        
        //Calculate the distance between zombie and player
        var distance = Math.sqrt((diff.x * diff.x) + (diff.y * diff.y));
        
        // Normalize
        var length = Math.sqrt(diff.x * diff.x + diff.y * diff.y);
        diff.x /= length; diff.y /= length;
        
        // if the player is close to us (but not to close) follow them
        if(distance <= 200) {
            if(distance > 32){
                
                this.sprite.rect.x += diff.x * (this.speed * game.deltaTime);
                this.sprite.rect.y += diff.y * (this.speed * game.deltaTime);
                
                // Set the zombies direction depending on what way its moving
                if(playerPos.x <= zombiePos.x)
                    this.sprite.direction.x = true;
                else
                    this.sprite.direction.x = false;
                    
                this.sprite.animator.Play('walk');
            }
            // If zombie is touching player, lower player health (You know simple zombie logic) :P
            else if(distance < 32){
                // Zombie hits player every second using a timer.
                if(this.canAttack){
                    game.player.health -= 10;
                    this.canAttack = false;
                    this.hitTimer.Reset();
                }
                this.sprite.animator.Play('idle');
            }
        }
        else{
            this.sprite.animator.Play('idle');
        }

        // Check if, when mouse is pressed is it on the zombie
        /*
        if(game.input.MousePressed(MouseButton.Left)){
            if(this.sprite.GetColliderRect().CheckPoint({x: mouse.x - game.camera.x, y: mouse.y - game.camera.y})){
                this.health -= 10;
                console.log(this.id + ' health: ' + this.health);
            }
            // Output health and speed for debugging
            console.log(this.health);
            console.log(this.speed);
        }
        // Round health up or down to always be at 100 or 0.
        if(this.health < 0) this.health = 0;
        if(this.health > 100) this.health = 100;
        
        if(game.player.health < 0) game.player.health = 0;
        if(game.player.health > 100) game.player.health = 100;
        
        // Zombie moves faster as health decreases
        if(this.health <= 75 && this.health > 50){
            this.speed = 0.08;
        }
        else if(this.health <= 50 && this.health > 25){
            this.speed = 0.1;
        }
        else if(this.health <= 25){
            this.speed = 0.12;
        }*/
        
        this.hitTimer.Tick();
        
        game.world.UpdateObject(this);
        
        this['super']('Update');
    },
    
    Render: function(){
        this['super']('Render');
    }
    
});