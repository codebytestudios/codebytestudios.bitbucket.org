/*global Class Building Sprite Animator Animation Frame SpriteManager Rect game KeyCode*/

var Fireplace = Class({extends: Building}, {
    __construct: function(id, x, y){
        
        this.sprite = SpriteManager.Create({
            rect: new Rect(x, y, 64, 64),
            collider: {rect: new Rect(0, 0, 64, 64), offset: {x: 0, y: 0}},
            dimensions: {width: 64, height: 64}, 
            origin: {x : 32, y : 32}, 
            layer: 100, 
            rotation: 0, 
            direction: {x: false, y: false},
            texture: game.textures['buildings'],
            animator: new Animator([
                new Animation("main", false, 5, [ new Frame(0, 0, 32, 32), new Frame(32, 0, 32, 32)])
            ])
        });
        this.sprite.animator.Play('main');
        
        this['super']('__construct', id);
    },
    
    Update: function(){
        this['super']('Update');
    },
    
    Render: function(){
        this['super']('Render');
    }
    
});